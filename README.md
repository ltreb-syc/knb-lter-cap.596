# knb-lter-cap.596

data publishing resources for knb-lter-cap.596 -- the 12K

*title*

Wetland abundance and distribution changes in Sycamore Creek, Arizona, USA (2014-2019)

*abstract*

The primary objective of this project is to understand how long-term climate variability and change influence the structure and function of desert streams via effects on hydrologic disturbance regimes. Climate and hydrology are intimately linked in arid landscapes; for this reason, desert streams are particularly well suited for both observing and understanding the consequences of climate variability and directional change. Researchers try to (1) determine how climate variability and change over multiple years influence stream biogeomorphic structure (i.e., prevalence and persistence of wetland and gravel-bed ecosystem states) via their influence on factors that control vegetation biomass, and (2) compare interannual variability in within-year successional patterns in ecosystem processes and community structure of primary producers and consumers of two contrasting reach types (wetland and gravel-bed stream reaches). This dataset was collected to understand: (1) the spatial pattern of wetland distribution and abundance; (2) the influence of multi-annual variability in hydrological regime on wetland distribution and abundance; and (3) the mechanism of the resilience of wetland to different disturbances in terms of hydrology (i.e., drying and flooding).

*notes*

- Sophia combined all 2014-2019 data into the combined PA and combined Patch files. The 2009-2013 data have a different structure and are missing critical pieces of information, such as they have GPS points but not coordinates. If and how to address pre 2014 data will have to be addressed with Nancy.
- Not seeing the metadata; have inquired with Sophia (2020-07-30).
- I added them as a separate sheet to those two files (similar to how some of our others are formatted) so you should be able to find them now. Hope all is well with you as well!
- These are also dataset 596.
- done: published as knb-lter-cap.596.2 in EDI
- only published 2010-2020; earlier years will require a separate effort owing to substantially different data structure and format
