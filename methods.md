[Annual 12K survey of Sycamore Creek Modified 6/15/2012]{.ul}

Objective and background: To conduct one survey per year during the
month of June (15 June + 1 week), the time of maximum vegetation biomass
and high interannual variability in cyanobacterial abundance, when
probability of flooding is low. In the survey, overall surface water
extent, wetland extent (by length), % cover of cyanobacteria (as
sentinels of N status) and % cover of wetland plants (as indicators of
ciénegas) at 25-m intervals will be estimated by four teams of two
surveyors. All records will be georeferenced at a resolution/accuracy of
\<2m using a handheld Trimble GeoXT GPS unit, and cross-referenced to
long-term study sites and geomorphic features mapped in past research.
These data will provide the basis for understanding how extreme events
may control the long-term persistence of wetland and gravel-bed reaches
as alternative stable states, as well as the nutrient-limitation status
of the ecosystem.

*Pre-survey reconnaissance*

-   Identify start and end points

-   Secure permissions to park

-   Gather Google maps or air photos, insert locations of monumented
    sites

-   Collect annual rock samples from locations: SS xing, AMW (10 per
    site)

*Survey tasks and variables*

1.  Georeferencing and recording

2.  Surface-water extent (linear) and wetland extent (linear)

3.  Individual wetland patches

    -   patch area, plant height, stem density, plant condition

    -   water depth , % in water

4.  Geomorphic data (major substrata in stream and channel)

5.  Reach characterization

6.  Cyanobacterial and wetland plant presence/absence (25-m intervals)

*Personnel: 20 people in four groups. Car arrangements*

Group 1 /Car 1 (5+1 people) arrive at RV, one person drives the car down
to BMW to join Group 4; Group 1 starts from RV. No car is left at RV.

Group 2 /Car 2 (5 people) arrive at SS; Group 2 starts from SS and goes
upstream towards RV. Leave Car 2 at SS; take keys to both cars (2 and
3).

Group 3/Car 3 (5 people) arrive at SS; Group 3 starts from SS and goes
downstream towards BMW. Leave Car 3 at SS (\*\*give key to Group 2
driver).

Group 4 /Car 4 (5-1 people) arrive at BMW and wait for the driver from
RV to join; Group 4 first goes downstream to finish the short segment
below BMW, then goes upstream towards SS. Leave Car 1 & 4 at BMW.

Groups 1 and 2 meet somewhere between RV and SS, and walk back towards
SS, where 2 cars are waiting (keys in possession of Group 2 driver).
Group 3 and 4 meet somewhere between SS and BMW and walk back towards
BMW, where 2 cars are waiting.

*Procedure*

Each Group has 5 people. 5 people are divided into 2 Teams.

Team 1 (2 people: 1 recorder, 1 measurer for presence/absence data)

Materials---

-   GPS

-   clipboard with data sheets for presence/absence (P/A)

-   pencils

-   meter stick

-   photos of wetland species

Procedures---

-   Start at known point, pace off 25 m. Observe along the length of the
    25 m to characterize the reach; record on sheet on same row as P/A
    data for the 25 m stopping point:

    -   Dry (no water)

    -   Open stream (water, but few macroalgaae or plants)

    -   Full of macroalgae (filamentous algae)

    -   Full of cyanobacteria

    -   Full of vascular plants

-   Stop, record GPS location, and viewing \~ a 1-m band across the
    stream (perpendicular), record:

    -   Channel type

    -   Water depth -- record 5 values across the stream

    -   Stream and channel substrata

    -   P/A of wetland plant species (see list below\*)

    -   P/A of cyanobacterial mats (*Anabaena*) or *Nostoc.*

-   Repeat.

-   Establish several points (every 200-400 m or so) where both GPS's
    are recorded at same spot; record waypoint of other GPS (labeled)
    under "notes"

-   Discontinue recording data at dry segments (unless plants are
    present); record location of surface water end as a GPS point.

-   Re-start presence/absence recording at tops of wet segments; record
    location.

Team 2 (3 people: 2 measure and record patch data, 1 density counts)

Materials---

-   GPS

-   clipboard with data sheet/pencils

-   maps and aerial photos with monumented sites marked

-   photos of wetland species

-   ziplocks for plant samples (for ID) (or press into note paper)

-   meter tape

-   light, 3-m rod for estimating plant height (marked at 0.5-m
    intervals)

-   meter stick to measure water depth

-   Camera, iPhone?

Procedures---

-   Determine start and end of surface water and wetland plant (ciénega)
    patches.

-   For each wetland patch \> 4m^2^ (i.e., \>2m x 2m), measure and
    record:

    -   GPS point at top and bottom of patch

    -   Dominant species of the patch. If other species are present at
        significant levels (\>10%) note under "other spp," except if
        they form a specific patch-within-a-patch (see below).

    -   length (if very long \[\>50 -- 100 m\], record GPS point at
        bottom of patch without measuring length)

    -   patch width -- average (in your head) of 3 measurements to
        nearest 0.5 m

    -   plant height -- three coarse measurements along the patch (not
        individual plants)

    -   Water depth -- measure at central point of the patch, if patch
        is entirely out of water, record as zero, if partially
        inundated, measure at the center of the part that is inundated.

    -   Water cover -- estimate and record the % of the patch that is
        inundated

    -   Plant condition -- record whether green/actively growing,
        brown/dying, or dead.

-   Density estimation -- depending on size of plants and density, count
    stems in 10cm x 10 cm (10), 25 cm x 25 cm (25), or 50 cm x 50
    cm (50) quadrats and record as \# counter/quadrat size (e.g.,
    10/10). ***Note: for 2012 survey, record this under "depth" -- so
    that column will have 3 values: z, n/q where z- depth at center,
    n=count, and q=quadrat size.***

-   Patch-within-a-patch: If there is a qualifying (\>2m x 2m) patch
    within a larger patch measured above, follow procedures as above,
    but under "notes" record "W" for within-patch. If the
    patch-within-a-patch starts at the same place as the larger patch,
    use the same "top" GPS point (similarly for ending at the same place
    as the larger patch). Unique GPS point should be added when the
    patch is embedded within the larger patch (example: TYDO 100 m
    length, GPS points 12 and 15, SCAM 20 m length, GPS points 13 and
    14, notes W).

-   Special case of patch-within-a-patch: two sides of the stream. If
    two sides of the stream have wetland plants but the patch does not
    completely cover the stream, it should be counted as one patch. The
    criterion for the end of a patch is 2 m free of plants in the
    longitudinal direction. If the dominant species on the two sides is
    different, then two patches should be measured but "W" recorded
    under "notes." GPS points would be the same.

-   Take photos and record notes

[Combined equipment list]{.ul}

-   8 hand-held GPS units

-   meter tapes (4)

-   light, 3-m rod for estimating plant height (marked at 0.5-m
    intervals)\* \--make (4)

-   ziplocks for plant samples (for ID)

-   4 clipboard with presence/absence data sheets

-   4 clipboards with wetland extent data sheets

-   16 pencils (4 for each group)

-   8 sharpies

-   maps and aerial photos with monumented sites marked (for each group)

-   1identification keys (for each group)

-   camera, iPhone with downloaded aerial imagery at all scales (for
    each group)

-   meter stick for water depth estimation (4)

[Wetland plant indicator species list -- see powerpoint]{.ul}

*PADI: Paspalum distichum --* knotgrass

*SCAM: Schoenoplectus americanus* -- chairmaker's bulrush

*EQLA: Equisetum laevigatum --* horsetail reed

*TYDO: Typha domingensis --* cattail

*SCACO2: Schoenoplectus acutus occidentalis* - Tule

*CYOD: Cyperus odoratus* - fragrant flatsedge

*JUTO: Juncus torreyi* - Torrey\'s rush

*PHAU*: *Phragmites australis*
